<?php

function generateRandomString($length = 80,$tipe='semua') {
    if($tipe == 'semua'){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else if($tipe == 'huruf_besar'){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else if($tipe == 'huruf_kecil'){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
    } else if($tipe == 'angka'){
        $characters = '0123456789';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
