<?php

function returnJSON($code,$message,$result, $http_code = false) {
	$out = [
        "message" => $message,
        "code"    => $code,
        "data"  => $result
    ];

    if(!$http_code) {
    	$http_code = $code;
    }

    return response()->json($out, $http_code);
}