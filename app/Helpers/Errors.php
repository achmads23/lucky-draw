<?php

function convertErrors($validator) {
	$err = array();
	foreach ($validator->errors()->toArray() as $error)  {
        foreach($error as $sub_error){
            array_push($err, $sub_error);
        }
    }
    return $err;

}