<?php

namespace App\Imports;

use App\User;
use App\Models\Undian;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UndianImport implements ToCollection
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    protected $_id = null; 

    public function __construct($id) { 
      $this->_id = $id; 
    }

    public function collection(Collection $rows)
    {
      foreach ($rows as $key => $row) 
      {
        if($key > 0){
          if($row[0]){
            $undian = Undian::where('nomor', $row[0])->where('id_kegiatan',$this->_id)->first();
            if($undian === null) {
              $undian = new Undian;
              $undian->id_kegiatan = $this->_id;
              $undian->nomor = $row[0];  
            }

            $undian->nama = $row[1];
            $undian->no_hp = isset($row[2]) ? $row[2] : '-';
            $undian->email = isset($row[3]) ? $row[3] : '-';
            $undian->alamat = isset($row[4]) ? $row[4] : '-';
            $undian->save();
          }
          
        }
          
      }
    }
}