<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $table = 'files';
	protected $appends = array('full_path');

    public function getFullPathAttribute(){
        return url($this->path);
    }
}
