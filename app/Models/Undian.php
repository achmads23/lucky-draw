<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Undian extends Model
{
	protected $table = 'undians';

	public function kegiatan()
    {
        return $this->hasOne('App\Models\Kegiatan', 'id','id_kegiatan');
    }
}
