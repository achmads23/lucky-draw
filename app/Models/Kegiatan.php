<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Kegiatan extends Model
{
    protected $table = 'kegiatans';
    protected $appends = array('path_background','tanggal_unix', 'created_at_unix');

    public function getPathBackgroundAttribute(){
        return url($this->background);
    }

    public function getTanggalUnixAttribute(){
        $date = new Carbon($this->tanggal);
        return $date->getTimestamp();
    }

    public function getCreatedAtUnixAttribute(){
        $date = new Carbon($this->created_at);
        return $date->getTimestamp();
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id','id_client');
    }

    public function undians()
    {
        return $this->hasMany('App\Models\Undian', 'id_kegiatan','id');
    }

    public function tahaps()
    {
        return $this->hasMany('App\Models\Tahap', 'id_kegiatan','id')->orderBy('urutan','ASC');
    }
}
