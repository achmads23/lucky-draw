<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tahap extends Model
{
	protected $table = 'tahaps';

	public function kegiatan()
    {
        return $this->hasOne('App\Models\Kegiatan', 'id','id_kegiatan');
    }

    public function pemenang()
    {
        return $this->hasMany('App\Models\Pemenang', 'id_tahap','id');
    }
}
