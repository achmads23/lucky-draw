<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $table = 'clients';

	public function users()
    {
        return $this->hasMany('App\User', 'id_client','id');
    }

    public function kegiatans()
    {
        return $this->hasMany('App\Model\Kegiatan', 'id_client','id');
    }
}
