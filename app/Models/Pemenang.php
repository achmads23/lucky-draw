<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemenang extends Model
{
	protected $table = 'pemenangs';

	public function tahap()
    {
        return $this->hasOne('App\Models\Tahap', 'id','id_tahap');
    }

    public function undian()
    {
        return $this->hasOne('App\Models\Undian', 'id','id_undian');
    }
}
