<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);

        require_once __DIR__ . '/../Helpers/String.php';
        require_once __DIR__ . '/../Helpers/Errors.php';
        require_once __DIR__ . '/../Helpers/Return.php';
        require_once __DIR__ . '/../Helpers/Path.php';
        require_once __DIR__ . '/../Constants/Role.php';
    }
}
