<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;

class ProfileController extends Controller
{
	public function index(Request $request)
    {
    	$user = Auth::user();
        return returnJSON(200, "success", $user);
    }
}