<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Tahap;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;

class TahapController extends Controller
{

	private $user;

	public function __construct()
    {
    	$this->user = Auth::user();
    }

    public function store(Request $request, $id_kegiatan)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
			'hadiah' => 'required',
			'jumlah_pemenang' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }

    	$tahap = new Tahap;

        $tahap->id_kegiatan = $id_kegiatan;
        $tahap->nama = $request->nama;
        $tahap->hadiah = $request->hadiah;
        $tahap->jumlah_pemenang = $request->jumlah_pemenang;

        if(!$request->urutan) {
            $tahaps = Tahap::where('id_kegiatan', $id_kegiatan)->get();
            $tahap->urutan = count($tahaps) + 1;
        } else {
            $tahap->urutan = $request->urutan;    
        }

        $tahap->status = 0;
		$tahap->save();
        return returnJSON(200, "success", $tahap);
    }

    public function show(Request $request, $id_kegiatan, $id)
    {
    	$tahap = Tahap::with('pemenang','pemenang.undian')->where('id',$id)->first();
    	if($tahap)
    		return returnJSON(200, "data found", $tahap);
    	else
    		return returnJSON(404, "not found", $tahap); 
    }

    public function update(Request $request, $id_kegiatan, $id)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
            'hadiah' => 'required',
            'jumlah_pemenang' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }


    	$tahap = Tahap::where('id',$id)->first();
    	if(!$tahap)
    		return returnJSON(404, "not found", $tahap); 

    	$tahap->id_kegiatan = $id_kegiatan;
        $tahap->nama = $request->nama;
        $tahap->hadiah = $request->hadiah;
        $tahap->jumlah_pemenang = $request->jumlah_pemenang;

        if(!$request->urutan) {
            $tahaps = Tahap::where('id_kegiatan', $id_kegiatan)->get();
            $tahap->urutan = count($tahaps) + 1;
        } else {
            $tahap->urutan = $request->urutan;    
        }

        $tahap->status = 0;
        $tahap->save();
        return returnJSON(200, "success", $tahap);
    }

    public function delete(Request $request, $id_kegiatan, $id)
    {
    	$tahap = Tahap::where('id',$id)->first();
    	if($tahap){
    		$tahap->delete();
    		return returnJSON(200, "data sucessfully deleted", null);
    	} else {
    		return returnJSON(404, "not found", $tahap); 
    	}
    }
}