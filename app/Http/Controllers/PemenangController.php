<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Undian;
use App\Models\Pemenang;
use App\Models\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Input;
use App\Imports\UndianImport;

class PemenangController extends Controller
{

	private $user;

	public function __construct()
    {
    	$this->user = Auth::user();
    }

    public function ganti(Request $request, $id_kegiatan, $id)
    {
        $pemenang = Pemenang::with('undian')->where('id',$id)->where('status',0)->first();   
        if(! $pemenang) return returnJSON(404, "not found", false);
        
        $undians = Undian::where('id_kegiatan',$id_kegiatan)->where('status',0)->pluck('id')->toArray();
        $id_pemenang = array_rand($undians,1);

        if($id_pemenang){
            $data = Undian::where('id',$id_pemenang)->first();
            $data->status = 1;
            $data->save();
            $pemenangs[] = $data;

            $pengganti = new Pemenang;
            $pengganti->id_kegiatan = $id_kegiatan;
            $pengganti->id_client = $this->user->id_client;
            $pengganti->id_tahap = $pemenang->id_tahap;
            $pengganti->id_undian = $id_pemenang;
            $pengganti->status = 0;
            $pengganti->save();

            $pemenang->status = -1;
            $pemenang->save();
        }

        $pengganti = $pengganti;

        return returnJSON(200, "request success", compact('pengganti'));
    }
}