<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Undian;
use App\Models\Pemenang;
use App\Models\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Input;
use App\Imports\UndianImport;

class UndianController extends Controller
{

	private $user;

	public function __construct()
    {
    	$this->user = Auth::user();
    }
    
	public function index(Request $request, $id_kegiatan)
    {
        $search = $request->search;
        $from = $request->from;
        $limit = $request->limit;
        $page = $request->page ?? 1;
        $filters = $request->filters;
        $sorts = $request->sorts;

    	$undians = Undian::where('id_kegiatan', $id_kegiatan);

        if(isset($request->search)){
            $like = $request->search;
            $undians = $undians->where(function ($query) use ($like)
            {
                $query->orWhere('nomor','like','%'.$like.'%')
                ->orWhere('nama','like','%'.$like.'%')
                ->orWhere('no_hp','like','%'.$like.'%')
                ->orWhere('alamat','like','%'.$like.'%')
                ->orWhere('email','like','%'.$like.'%');
            });
        }
        
        if (!empty($sorts) && $sorts != null) {
            foreach ($sorts as $index) {
                foreach ($index as $k => $value) {
                    $undians = $undians->orderBy($k, $value);
                }
            }
        }
        else{
            $undians =  $undians->orderBy('created_at', 'ASC');
        }

        if (!isset($limit) && empty($limit)) {
            $limit = 10;
        }

        $undians= $undians->paginate($limit, ['*'], 'page', $page)->toArray();

        return returnJSON(200, "success", $undians);
    }

    public function ganti_pemenang(Request $request, $id_kegiatan, $id)
    {
        $pemenang = Pemenang::with('undian')->where('id',$id)->first();   
        $undians = Undian::where('id_kegiatan',$id)->where('status',0)->pluck('id')->toArray();
        $id_pemenangs = array_rand($undians,1);

        if($id_pemenangs > 0){
            $data = Undian::where('id',$id_pemenangs[0])->first();
            $data->status = 1;
            $data->save();
            $pemenangs[] = $data;

            $winner = new Pemenang;
            $winner->id_kegiatan = $id_kegiatan;
            $winner->id_client = $this->user->id_client;
            $winner->id_tahap = $pemenang->id_tahap;
            $winner->id_undian = $id_pemenangs[0];
            $winner->status = 0;
            $winner->save();

            $pemenang->status = -1;
            $pemenang->save();
        }
    }

    // Belum Disesuaikan
    public function store(Request $request, $id_kegiatan)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
			'tema' => 'required',
			'tanggal' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }

    	$undian = new Undian;
    	$undian->nama = $request->nama;
		$undian->tema = $request->tema;
		$undian->background = $request->background ?? 'kegiatan/background.jpg';
		$undian->tanggal = Carbon::createFromTimestamp($request->tanggal);
		$undian->id_client = $this->user->id_client;
		$undian->save();
        return returnJSON(200, "success", $undian);
    }

    public function show(Request $request, $id_kegiatan, $id)
    {
    	$undian = Undian::with('tahaps','tahaps.pemenang','tahaps.pemenang.undian')->where('id',$id)->where('id_client', $this->user->id_client)->first();
    	if($undian)
    		return returnJSON(200, "data found", $undian);
    	else
    		return returnJSON(404, "not found", null); 
    }

    public function update(Request $request, $id_kegiatan, $id)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
			'tema' => 'required',
			'tanggal' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }


    	$undian = Undian::where('id',$id)->first();
    	if(!$undian)
    		return returnJSON(404, "not found", null); 

    	$undian->nama = $request->nama;
		$undian->tema = $request->tema;
		$undian->background = $request->background ?? 'kegiatan/background.jpg';
		$undian->tanggal = Carbon::createFromTimestamp($request->tanggal);
		$undian->id_client = $this->user->id_client;
		$undian->save();
        return returnJSON(200, "success", $undian);
    }

    public function delete(Request $request, $id_kegiatan, $id)
    {
    	$undian = Undian::where('id',$id)->where('id_client', $this->user->id_client)->first();
    	if($undian){
    		$undian->delete();
    		return returnJSON(200, "data sucessfully deleted", null);
    	} else {
    		return returnJSON(404, "not found", null); 
    	}
    }
}