<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;

class UploadController extends Controller
{
	public function store(Request $request)
    {
    	$user = Auth::user();

  		$file = $request->file('file');
  		$file->store('uploads');
      
    	$f = new File;
    	$f->name = $file->getClientOriginalName();
    	$f->extension = $file->getClientOriginalExtension();
    	$f->mime = $file->getMimeType();
    	$f->path = 'storage/uploads/' . $file->hashName();
	    $f->save();

      return returnJSON(200, "success", $f);
    }
}