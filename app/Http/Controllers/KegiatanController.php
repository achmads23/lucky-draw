<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Kegiatan;
use App\Models\Tahap;
use App\Models\Undian;
use App\Models\Pemenang;
use App\Models\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Input;
use App\Imports\UndianImport;

class KegiatanController extends Controller
{

	private $user;

	public function __construct()
    {
    	$this->user = Auth::user();
    }
    
	public function index(Request $request)
    {
        $search = $request->search;
        $from = $request->from;
        $limit = $request->limit;
        $page = $request->page ?? 1;
        $filters = $request->filters;
        $sorts = $request->sorts;

    	$kegiatans = Kegiatan::where('id_client', $this->user->id_client);

        if(isset($request->search)){
            $like = $request->search;
            $kegiatans = $kegiatans->where(function ($query) use ($like)
            {
                $query->orWhere('tema','like','%'.$like.'%')
                ->orWhere('nama','like','%'.$like.'%')
                ->orWhere('tanggal','like','%'.$like.'%');
            });
        }

        if (!empty($sorts) && $sorts != null) {
            foreach ($sorts as $index) {
                foreach ($index as $k => $value) {
                    $kegiatans = $kegiatans->orderBy($k, $value);
                }
            }
        }
        else{
            $kegiatans =  $kegiatans->orderBy('created_at', 'DESC');
        }

        if (!isset($limit) && empty($limit)) {
            $limit = 10;
        }

        $kegiatans= $kegiatans->paginate($limit, ['*'], 'page', $page)->toArray();

        return returnJSON(200, "success", $kegiatans);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
			'tema' => 'required',
			'tanggal' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }

    	$kegiatan = new Kegiatan;
    	$kegiatan->nama = $request->nama;
		$kegiatan->tema = $request->tema;
		$kegiatan->background = $request->background ?? 'kegiatan/background.jpg';
		$kegiatan->tanggal = Carbon::createFromTimestamp($request->tanggal);
		$kegiatan->id_client = $this->user->id_client;
		$kegiatan->save();
        return returnJSON(200, "success", $kegiatan);
    }

    public function show(Request $request, $id)
    {
    	$kegiatan = Kegiatan::with('tahaps','tahaps.pemenang','tahaps.pemenang.undian')->where('id',$id)->where('id_client', $this->user->id_client)->first();
    	if($kegiatan)
    		return returnJSON(200, "data found", $kegiatan);
    	else
    		return returnJSON(404, "not found", null); 
    }

    public function undian(Request $request, $id)
    {
        $kegiatan = Kegiatan::with('tahaps','tahaps.pemenang','tahaps.pemenang.undian')->where('id',$id)->where('id_client', $this->user->id_client)->first();
        if($kegiatan)
            return returnJSON(200, "data found", $kegiatan);
        else
            return returnJSON(404, "not found", null); 
    }

    public function update(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
            'nama' => 'required',
			'tema' => 'required',
			'tanggal' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "unporcessable entity", convertErrors($validator));
        }


    	$kegiatan = Kegiatan::where('id',$id)->first();
    	if(!$kegiatan)
    		return returnJSON(404, "not found", null);

    	$kegiatan->nama = $request->nama;
		$kegiatan->tema = $request->tema;
        if($request->background){
            $kegiatan->background = $request->background;    
        }
		$kegiatan->tanggal = Carbon::createFromTimestamp($request->tanggal);
		$kegiatan->id_client = $this->user->id_client;
		$kegiatan->save();
        return returnJSON(200, "success", $kegiatan);
    }

    public function delete(Request $request, $id)
    {
    	$kegiatan = Kegiatan::where('id',$id)->where('id_client', $this->user->id_client)->first();
    	if($kegiatan){
    		$kegiatan->delete();
    		return returnJSON(200, "data sucessfully deleted", null);
    	} else {
    		return returnJSON(404, "not found", null); 
    	}
    }

    public function upload_undian(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        $excel = File::where('id',$request->id)->first();
        if(!$excel)
            return returnJSON(404, "not found", null); 

        $array = Excel::import(new UndianImport($id), public_path($excel->path));

        return returnJSON(200, "import success", null);
    }

    public function pre_play(Request $request, $id){
        $kegiatan = Kegiatan::where('id',$id)->where('id_client', $this->user->id_client)->first();
        $tahaps = Tahap::with('pemenang','pemenang.undian')->where('id_kegiatan',$id)->where('status',0)->orderBy('urutan', 'ASC')->get();
        if($kegiatan){
            return returnJSON(200, "request success", compact('kegiatan','tahaps'));
        } else {
            return returnJSON(404, "not found", null); 
        }
    }

    public function play(Request $request, $id){
        $kegiatan = Kegiatan::where('id',$id)->where('id_client', $this->user->id_client)->first();
        $tahap = Tahap::where('id_kegiatan',$id)->where('status',0)->orderBy('urutan', 'ASC')->first();
        $undians = Undian::where('id_kegiatan',$id)->where('status',0)->pluck('id')->toArray();
        $id_arrays = array_rand($undians,$tahap->jumlah_pemenang);

        $id_pemenangs = [];
        if(!is_array($id_arrays)) {
            $id_pemenangs = [$id_arrays];
        } else {
            $id_pemenangs = $id_arrays;
        }

        $pemenangs = [];
        foreach ($id_pemenangs as $key => $id_pemenang) {
            $data = Undian::where('id',$undians[$id_pemenang])->first();
            $data->status = 1;
            $data->save();
            $pemenangs[] = $data;

            $winner = new Pemenang;
            $winner->id_kegiatan = $id;
            $winner->id_client = $this->user->id_client;
            $winner->id_tahap = $tahap->id;
            $winner->id_undian = $undians[$id_pemenang];
            $winner->status = 0;
            $winner->save();
        }

        $tahap->status = 1;
        $tahap->save();

        if($kegiatan){
            return returnJSON(200, "request success", compact('kegiatan','tahap','pemenangs'));
        } else {
            return returnJSON(404, "not found", null); 
        }
    }

    public function post_play(Request $request, $id){
        $kegiatan = Kegiatan::with('tahaps','tahaps.pemenang','tahaps.pemenang.undian')->where('id',$id)->where('id_client', $this->user->id_client)->first();
        if($kegiatan){
            return returnJSON(200, "request success", compact('kegiatan'));
        } else {
            return returnJSON(404, "not found", null); 
        }
    }

    public function reset(Request $request, $id)
    {
        Undian::where('id_kegiatan', $id)->update(['status' => 0]);
        Tahap::where('id_kegiatan', $id)->update(['status' => 0]);
        Pemenang::where('id_kegiatan', $id)->delete();
        return returnJSON(200, "request success", null);
    }

    public function permanent(Request $request, $id)
    {
        $kegiatan = Kegiatan::where('id',$id)->first();
        if(!$kegiatan)
            return returnJSON(404, "not found", null); 

        $kegiatan->status = 1;
        $kegiatan->save();
        return returnJSON(200, "success", $kegiatan);
    }

    public function switch_music(Request $request, $id)
    {
        $kegiatan = Kegiatan::where('id',$id)->first();
        if(!$kegiatan)
            return returnJSON(404, "not found", null); 

        $kegiatan->lagu = $kegiatan->lagu ? 0 : 1;
        $kegiatan->save();
        return returnJSON(200, "success", $kegiatan);
    }

    public function switch_nomor_undian(Request $request, $id)
    {
        $kegiatan = Kegiatan::where('id',$id)->first();
        if(!$kegiatan)
            return returnJSON(404, "not found", null); 

        $kegiatan->nomor_undian = $kegiatan->nomor_undian ? 0 : 1;
        $kegiatan->save();
        return returnJSON(200, "success", $kegiatan);
    }
}