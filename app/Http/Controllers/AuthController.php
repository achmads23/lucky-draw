<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use Auth;
class AuthController extends Controller
{

    public function register(Request $request)
    {
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|min:5'
        ]);

        if ($validator->fails()) {
            return returnJSON(422, "login-failed", convertErrors($validator));
        }

        $email = $request->input("email");
        $password = $request->input("password");
 
        $user = User::where("email", $email)->first();
        $out = returnJSON(400, "login-failed", ['invalid email and/or password']);

        if ($user) {
            if (Hash::check($password, $user->password)) {
                do {
                    $newtoken  = generateRandomString();
                    try {
                        $return = $user->update([
                            'token' => $newtoken
                        ]);    
                    } catch (Exception $e) {
                        continue;
                    }
                } while (!$return);
                $out = returnJSON(200, "login-success", $user);
            }
        }
        return $out;
    }

    public function logout()
    {
        $user = Auth::user();
        $return = $user->update(['token' => null]); 
        return returnJSON(200, "success", $user);
    }
}