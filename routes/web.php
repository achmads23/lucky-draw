<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post("/login", "AuthController@login");

$router->group(['middleware' => ['auth']], function () use ($router){
	$router->get("/logout", "AuthController@logout");


	$router->get("/profile", "ProfileController@index");
	$router->get("/kegiatans", "KegiatanController@index");
	$router->post("/kegiatans", "KegiatanController@store");
	$router->post("/kegiatans/{id}/reset", "KegiatanController@reset");
	$router->post("/kegiatans/{id}/permanent", "KegiatanController@permanent");
	$router->post("/kegiatans/{id}/switch-music", "KegiatanController@switch_music");
	$router->post("/kegiatans/{id}/switch-nomor-undian", "KegiatanController@switch_nomor_undian");
	$router->put("/kegiatans/{id}", "KegiatanController@update");
	$router->get("/kegiatans/{id}", "KegiatanController@show");
	$router->delete("/kegiatans/{id}", "KegiatanController@delete");

	$router->post("/kegiatans/{id_kegiatan}/tahaps", "TahapController@store");
	$router->put("/kegiatans/{id_kegiatan}/tahaps/{id}", "TahapController@update");
	$router->get("/kegiatans/{id_kegiatan}/tahaps/{id}", "TahapController@show");
	$router->delete("/kegiatans/{id_kegiatan}/tahaps/{id}", "TahapController@delete");

	$router->get("/kegiatans/{id_kegiatan}/undians", "UndianController@index");

	$router->post("/kegiatans/{id}/upload-undian", "KegiatanController@upload_undian");
	$router->get("/kegiatans/{id}/pre-play", "KegiatanController@pre_play");

	$router->post("/kegiatans/{id}/play", "KegiatanController@play");
	$router->post("/kegiatans/{id_kegiatan}/pemenangs/{id}/ganti", "PemenangController@ganti");

	$router->post("/upload", "UploadController@store");
});
