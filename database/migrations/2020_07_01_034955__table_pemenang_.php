<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablePemenang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemenangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_client");
            $table->integer("id_kegiatan");
            $table->integer("id_tahap");
            $table->integer("id_undian");
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemenangs');
    }
}
