<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableTahap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahaps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_kegiatan");
            $table->integer("urutan")->default(0);
            $table->string("nama");
            $table->string("hadiah");
            $table->integer("jumlah_pemenang");
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahaps');
    }
}
