<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableUndian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_kegiatan");
            $table->string("nomor");
            $table->string("nama")->nullable();
            $table->string("no_hp")->nullable();
            $table->string("email")->nullable();
            $table->text("alamat")->nullable();
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('undians');
    }
}
